﻿/**
 * The Class OvrAvatarHand
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ProtocolLearning;

public class OvrAvatarHand : MonoBehaviour
{
    public HandController m_HandController;
    Rigidbody m_RigidBody = null;
    OVRInput.Controller m_Controller;

    private const string LEFTHAND = "left";

    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        if (name.ToLower().Contains(LEFTHAND))
        {
            m_Controller = OVRInput.Controller.LTouch;
        }
        else
        {
            m_Controller = OVRInput.Controller.RTouch;
        }
    }

    public void Update()
    {
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_Controller) >= 0.75f)
        {
            if (m_HandController.m_HandState != HandController.HandState.HOLDING)
            {
                m_RigidBody.detectCollisions = true;
                m_HandController.m_HandState = OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger, m_Controller) ? HandController.HandState.PUSHING : HandController.HandState.TOUCHING;
            }
        }
        else
        {
            m_RigidBody.detectCollisions = false;
            if (m_HandController.m_HandState != HandController.HandState.HOLDING)
            {
                m_HandController.m_HandState = HandController.HandState.EMPTY;
            }
        }
    }
}