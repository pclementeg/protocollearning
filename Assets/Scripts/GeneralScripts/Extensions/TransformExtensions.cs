﻿/**
 * The Class TransformExtensions
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// TransformExtensions for generic uses
/// </summary>
public static class TransformExtensions 
{
    /// <summary>
    /// Set X position (world coordinates)
    /// </summary>
    /// <param name="t">this transform</param>
    /// <param name="x">new X position</param>
	public static void SetX(this Transform t, float x) 
    {
		Vector3 newPosition = new Vector3(x, t.position.y, t.position.z);
		t.position = newPosition;
	}

    /// <summary>
    /// Set Y Position (world coordinates)
    /// </summary>
    /// <param name="t">this transform</param>
    /// <param name="y">new Y position</param>
	public static void SetY(this Transform t, float y) 
    {
		Vector3 newPosition = new Vector3(t.position.x, y, t.position.z);
		t.position = newPosition;
	}

    /// <summary>
    /// Set Z Position (world coordinates)
    /// </summary>
    /// <param name="t">this transform</param>
    /// <param name="z">new Z position</param>
	public static void SetZ(this Transform t, float z) 
    {
		Vector3 newPosition = new Vector3(t.position.x, t.position.y, z);
		t.position = newPosition;
	}

    public static void SetLocalX(this Transform t, float x)
    {
        Vector3 newPosition = new Vector3(x, t.localPosition.y, t.localPosition.z);
        t.localPosition = newPosition;
    }

    public static void SetLocalY(this Transform t, float y)
    {
        Vector3 newPosition = new Vector3(t.localPosition.x, y, t.localPosition.z);
        t.localPosition = newPosition;
    }

    public static void SetLocalZ(this Transform t, float z)
    {
        Vector3 newPosition = new Vector3(t.localPosition.x, t.localPosition.y, z);
        t.localPosition = newPosition;
    }

    public static float GetX(this Transform t) 
    {
		return t.position.x;
	}
	
	public static float GetY(this Transform t) 
    {
		return t.position.y;
	}
	
	public static float GetZ(this Transform t) 
    {
		return t.position.z;
	}

    public static float GetLocalX(this Transform t)
    {
        return t.localPosition.x;
    }

    public static float GetLocalY(this Transform t)
    {
        return t.localPosition.y;
    }

    public static float GetLocalZ(this Transform t)
    {
        return t.localPosition.z;
    }

    //translate locals
    public static void TranslateLocalX(this Transform t, float x) 
    {
		t.Translate(x, 0.0f, 0.0f, Space.Self);
	}

	public static void TranslateLocalY(this Transform t, float y) 
    {
		t.Translate(0.0f, y, 0.0f, Space.Self);
	}

	public static void TranslateLocalZ(this Transform t, float z) 
    {
		t.Translate(0.0f, 0.0f, z, Space.Self);
	}

    //translate to parent
    public static void TranslateParentX(this Transform t, float x)
    {
        t.Translate(x, 0.0f, 0.0f, t.parent);
    }

    public static void TranslateParentY(this Transform t, float y)
    {
        t.Translate(0.0f, y, 0.0f, t.parent);
    }

    public static void TranslateParentZ(this Transform t, float z)
    {
        t.Translate(0.0f, 0.0f, z, t.parent);
    }

    //translate world
    public static void TranslateWorldXY(this Transform t, float x, float y) 
    {
		t.Translate(x, y, 0.0f, Space.World);
	}
	
	public static void TranslateWorldXZ(this Transform t, float x, float z) 
    {
		t.Translate(x, 0.0f, z, Space.World);
	}
	
	public static void TranslateWorldYZ(this Transform t, float y, float z) 
    {
		t.Translate(0.0f, y, z, Space.World);
	}

	//rotate
	public static void RotateX(this Transform t, float x) 
    {
		t.Rotate(x,0.0f,0.0f);
	}

	public static void RotateY(this Transform t, float y) 
    {
		t.Rotate(0.0f,y,0.0f);
	}

	public static void RotateZ(this Transform t, float z) 
    {
		t.Rotate(0.0f,0.0f,z);
	}

    //local scale
    public static void SetLocalScaleXY(this Transform t, float x, float y)
    {
        Vector3 newScale = new Vector3(x, y, t.localScale.z);
        t.localScale = newScale;
    }

    public static void SetLocalScaleXZ(this Transform t, float x, float z)
    {
        Vector3 newScale = new Vector3(x,  t.localScale.y, z);
        t.localScale = newScale;
    }

    public static void SetLocalScaleY(this Transform t, float y)
    {
        Vector3 newScale = new Vector3(t.localScale.x,  y, t.localScale.z);
        t.localScale = newScale;
    }

    /// <summary>
    /// Find all children of the Transform by tag (includes self)
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="tags"></param>
    /// <returns></returns>
    public static List<Transform> FindChildrenByTag(this Transform transform, params string[] tags)
    {
        List<Transform> list = new List<Transform>();
        foreach (var tran in transform.Cast<Transform>().ToList())
            list.AddRange(tran.FindChildrenByTag(tags)); // recursively check children
        if (tags.Any(tag => tag == transform.tag))
            list.Add(transform); // we matched, add this transform
        return list;
    }

    /// <summary>
    /// Find all children of the GameObject by tag (includes self)
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="tags"></param>
    /// <returns></returns>
    public static List<GameObject> FindChildrenByTag(this GameObject gameObject, params string[] tags)
    {
        return FindChildrenByTag(gameObject.transform, tags)
            //.Cast<GameObject>() // Can't use Cast here :(
            .Select(tran => tran.gameObject)
            .Where(gameOb => gameOb != null)
            .ToList();
    }
}
