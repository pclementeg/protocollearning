﻿/**
 * The Class Handle
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System;
using System.Collections;
using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public abstract class Handle : OVRGrabbable
    {
        public OpenCloseAction.State m_State = OpenCloseAction.State.Close;
        [SerializeField]
        protected Transform m_HandlePivot;
        protected Transform m_CloseTransform;
        protected Vector3 m_HandleOffsetPosition;
        protected Quaternion m_HandleOffsetRotation;

        public event Action OnGrabBegin;
        public event Action OnGrabEnd;


        protected new void Start()
        {
            base.Start();
        }

        public virtual void Initialize(OpenCloseAction.State startingState)
        {

        }

        override public void GrabBegin(OVRGrabber hand, Collider grabPoint)
        {
            base.GrabBegin(hand, grabPoint);
            DoMovement();
            if (OnGrabBegin != null)
                OnGrabBegin();
        }

        protected virtual void DoMovement()
        {

        }

        override public void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
        {
            base.GrabEnd(linearVelocity, angularVelocity);
            if (OnGrabEnd != null)
                OnGrabEnd();
        }

        protected virtual IEnumerator MovePositive()
        {
            yield return new WaitForEndOfFrame();
        }

        protected virtual IEnumerator MoveNegative()
        {
            yield return new WaitForEndOfFrame();
        }

        protected void EndMovement()
        {
            this.transform.parent = m_HandlePivot;
            this.transform.localPosition = m_HandleOffsetPosition;
            this.transform.localRotation = m_HandleOffsetRotation;
        }
    }
}
