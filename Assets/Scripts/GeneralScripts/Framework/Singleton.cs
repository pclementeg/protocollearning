﻿/**
 * The Class Singleton
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T instance;

        /**
          Returns the instance of this singleton.
       */
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));
#if UNITY_EDITOR
                    if (instance == null)
                    {
                        Debug.LogError("An instance of " + typeof(T) + " is needed, but is not found on Scene.");
                    }
#endif
                }
                return instance;
            }
        }

        protected void AwakeSingleton(T singleton, bool dontdestroyonload)
        {
            if (instance == null)
            {
                instance = singleton;
#if !UNITY_EDITOR
            if (dontdestroyonload)
                DontDestroyOnLoad(instance.gameObject);
#endif
            }
            else
            {
#if !UNITY_EDITOR
            Destroy(singleton.gameObject);
#endif
            }
        }

        protected void OnApplicationQuit()
        {
#if !UNITY_EDITOR
        instance = null;
#endif
        }
    }
}
