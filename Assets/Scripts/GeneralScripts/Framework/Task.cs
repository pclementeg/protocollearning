﻿/**
 * The Class Task
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using UnityEngine;

namespace ProtocolLearning
{
    public abstract class Task : MonoBehaviour
    {
        protected ProtocolManager m_ProtocolManager;
        [SerializeField]
        protected StepAction m_StepAction;
        public Condition[] m_Conditions = new Condition[0];
        public Consequence[] m_SuccessConsequences = new Consequence[0];
        public Consequence[] m_FailConsequences = new Consequence[0];

        protected void Start()
        {
            m_ProtocolManager = ProtocolManager.Instance;
            m_StepAction.Initialize(this.gameObject);

            for (int i = 0; i < m_Conditions.Length; i++)
            {
                m_Conditions[i].m_Hash = Animator.StringToHash(m_Conditions[i].m_Description);
            }

            for (int i = 0; i < m_SuccessConsequences.Length; ++i)
            {
                DelayedConsequence delayedConsequence = m_SuccessConsequences[i] as DelayedConsequence;

                if (delayedConsequence)
                {
                    delayedConsequence.Initialization(this.gameObject);
                }
                else
                {
                    m_SuccessConsequences[i].Initialization(this.gameObject);
                }
            }

            for (int i = 0; i < m_FailConsequences.Length; ++i)
            {
                DelayedConsequence delayedConsequence = m_FailConsequences[i] as DelayedConsequence;

                if (delayedConsequence)
                {
                    delayedConsequence.Initialization(this.gameObject);
                }
                else
                {
                    m_FailConsequences[i].Initialization(this.gameObject);
                }
            }
        }

        protected bool CheckConditions()
        {
            for (int i = 0; i < m_Conditions.Length; i++)
            {
                if (!ProtocolManager.CheckCondition(m_Conditions[i]))
                    return false;
            }
            return true;
        }

        protected void React()
        {
            if (CheckConditions())
            {
                m_ProtocolManager.StepActionDone(m_StepAction);
                GoodConsequences();
            }
            else
            {
                FailConsequences();
            }
        }

        public void GoodConsequences()
        {
            for (int i = 0; i < m_SuccessConsequences.Length; ++i)
            {
                DelayedConsequence delayedConsequence = m_SuccessConsequences[i] as DelayedConsequence;

                if (delayedConsequence)
                {
                    delayedConsequence.React(this);
                }
                else
                {
                    m_SuccessConsequences[i].React(this);
                }
            }
        }

        public void FailConsequences()
        {
            for (int i = 0; i < m_FailConsequences.Length; ++i)
            {
                DelayedConsequence delayedConsequence = m_FailConsequences[i] as DelayedConsequence;

                if (delayedConsequence)
                {
                    delayedConsequence.React(this);
                }
                else
                {
                    m_FailConsequences[i].React(this);
                }
            }
        }
    }
}
