﻿/**
 * The Class TestData
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ProtocolLearningData
{
    //[Serializable]
    //public enum ProtocolEvaluation
    //{
    //    Success,
    //    Fail,
    //    Incomplete
    //}; 

    [Serializable]
    public class TestData
    {
        public string protocolName;
        public float protocolTime;
        public ProtocolEvaluation protocolEvaluation;
        public FailReason failReason;
        public string lastStepName;
        public int lastStepIndex;
        public bool isFinished;
        public DateTime dateTime;

        public TestData(string name, float time, ProtocolEvaluation evaluation, FailReason fail, string stepName, int stepIndex, bool isfinished, DateTime date)
        {
            protocolName = name;
            protocolTime = time;
            protocolEvaluation = evaluation;
            failReason = fail;
            lastStepName = stepName;
            lastStepIndex = stepIndex;
            isFinished = isfinished;
            dateTime = date;
        }
    }
}
