﻿/**
 * The Class Glow
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MeshRenderer))]
    [AddComponentMenu("ProtocolLearning/Glow")]
    public class Glow : MonoBehaviour
    {
        [SerializeField]
        private Renderer m_Renderer;
        public Color m_TintedColor;
        private Color m_NormalColor;
        private Color m_CurrentColor;
        public float m_WaitTime;
        private MaterialPropertyBlock m_MaterialPropertyBlock;
        public const string EMISSION_COLOR = "_EmissionColor";
        public const string EMISSION = "_Emission";

        void Awake()
        {
            m_Renderer = GetComponent<Renderer>();
            m_MaterialPropertyBlock = new MaterialPropertyBlock();
        }

        private void Start()
        {
            m_NormalColor = m_MaterialPropertyBlock.GetColor(Shader.PropertyToID(EMISSION_COLOR));
            m_CurrentColor = m_NormalColor;
            OnValidate();
            m_Renderer.sharedMaterial.EnableKeyword(EMISSION);
        }

        void OnValidate()
        {
            m_Renderer = GetComponent<Renderer>();
            m_MaterialPropertyBlock = new MaterialPropertyBlock();
            m_Renderer.GetPropertyBlock(m_MaterialPropertyBlock);
            m_MaterialPropertyBlock.SetColor(Shader.PropertyToID(EMISSION_COLOR), m_CurrentColor);
            m_Renderer.SetPropertyBlock(m_MaterialPropertyBlock);
        }

        public void GlowEffect()
        {
            StartCoroutine(GlowTimed());
        }

        private IEnumerator GlowTimed()
        {
            m_CurrentColor = m_TintedColor;
            OnValidate();
            yield return new WaitForSeconds(m_WaitTime);
            m_CurrentColor = m_NormalColor;
            OnValidate();
        }
    }
}
