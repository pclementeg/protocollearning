﻿/**
 * The Class PlayerController
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CharacterController))]
    [AddComponentMenu("ProtocolLearning/ProtocolLearning Player Controller")]
    public class PlayerController : OVRPlayerController
    {
        [SerializeField]
        private OVRPlayerController m_OVRPlayerController;
        //OVRPlayer Management
        [SerializeField]
        private OVRCameraRig m_OVRCameraRig;
        private OVRPose? m_InitialPose;
        private float m_PendingRotation = 0f;
        private float m_AxisDeadZone = 0.1f;
        private float m_RotationAnimation = 0f;
        private const float SIMULATIONRATE = 60f;
        private float m_SimulationRate = 0f;
        private bool m_Animating = false;
        private float m_TargetYaw = 0f;
        private float m_FallSpeed = 0.0f;
        private Vector3 MoveControl = Vector3.zero;
        public OVRInput.Button m_RunButton = OVRInput.Button.One;
        public OVRInput.Button m_AlternateRunButton = OVRInput.Button.PrimaryThumbstick;

        private const string MOUSEX = "Mouse X";

        private void Awake()
        {
            Controller = gameObject.GetComponent<CharacterController>();
        }


        // Use this for initialization
        private void Start()
        {
            CameraRig = m_OVRCameraRig;
        }

        private float AngleDifference(float a, float b)
        {
            float diff = (360 + a - b) % 360;
            if (diff > 180)
                diff -= 360;
            return diff;
        }

        private void OnEnable()
        {
            m_OVRPlayerController.CameraUpdated += _cameraUpdateAction;
            m_OVRPlayerController.PreCharacterMove += _preCharacterMovementAction;
        }

        private void OnDisable()
        {
            m_OVRPlayerController.PreCharacterMove -= _preCharacterMovementAction;
            m_OVRPlayerController.CameraUpdated -= _cameraUpdateAction;
        }

        private void _cameraUpdateAction()
        {
            Debug.Log("_cameraUpdateAction");
        }

        private void _preCharacterMovementAction()
        {
            Debug.Log("_preCharacterMovementAction");
        }

        private void Update()
        {
            m_SimulationRate = Time.deltaTime * SIMULATIONRATE;
            if (!ApplicationManager.m_OnMenu)
            {
                UpdateController();
            }
        }

        protected override void UpdateController()
        {
            if (useProfileData)
            {
                if (m_InitialPose == null)
                {
                    // Save the initial pose so it can be recovered if useProfileData
                    // is turned off later.
                    m_InitialPose = new OVRPose()
                    {
                        position = CameraRig.transform.localPosition,
                        orientation = CameraRig.transform.localRotation
                    };
                }

                var p = CameraRig.transform.localPosition;
                if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.EyeLevel)
                {
                    p.y = OVRManager.profile.eyeHeight - (0.5f * Controller.height) + Controller.center.y;
                }
                else if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.FloorLevel)
                {
                    p.y = -(0.5f * Controller.height) + Controller.center.y;
                }
                CameraRig.transform.localPosition = p;
            }
            else if (m_InitialPose != null)
            {
                // Return to the initial pose if useProfileData was turned off at runtime
                CameraRig.transform.localPosition = m_InitialPose.Value.position;
                CameraRig.transform.localRotation = m_InitialPose.Value.orientation;
                m_InitialPose = null;
            }

            CameraHeight = CameraRig.centerEyeAnchor.localPosition.y;

            //if (CameraUpdated != null)
            //{
            //    CameraUpdated();
            //}

            UpdateMovement();

            Vector3 moveDirection = Vector3.zero;

            float motorDamp = (1.0f + (Damping * m_SimulationRate));

            MoveControl.x /= motorDamp;
            MoveControl.y = (MoveControl.y > 0.0f) ? (MoveControl.y / motorDamp) : MoveControl.y;
            MoveControl.z /= motorDamp;

            moveDirection += MoveControl * m_SimulationRate;

            // Gravity
            if (Controller.isGrounded && m_FallSpeed <= 0)
            {
                m_FallSpeed = ((Physics.gravity.y * (GravityModifier * 0.002f)));
            }
            else
            {
                m_FallSpeed += ((Physics.gravity.y * (GravityModifier * 0.002f)) * m_SimulationRate);
            }

            moveDirection.y += m_FallSpeed * m_SimulationRate;


            if (Controller.isGrounded && MoveControl.y <= transform.lossyScale.y * 0.001f)
            {
                // Offset correction for uneven ground
                float bumpUpOffset = Mathf.Max(Controller.stepOffset, new Vector3(moveDirection.x, 0, moveDirection.z).magnitude);
                moveDirection -= bumpUpOffset * Vector3.up;
            }

            //if (PreCharacterMove != null)
            //{
            //    PreCharacterMove();
            //    Teleported = false;
            //}

            Vector3 predictedXZ = Vector3.Scale((Controller.transform.localPosition + moveDirection), new Vector3(1, 0, 1));

            // Move contoller
            Controller.Move(moveDirection);
            Vector3 actualXZ = Vector3.Scale(Controller.transform.localPosition, new Vector3(1, 0, 1));

            if (predictedXZ != actualXZ)
                MoveControl += (actualXZ - predictedXZ) / m_SimulationRate;
        }

        public override void UpdateMovement()
        {
            bool HaltUpdateMovement = false;
            GetHaltUpdateMovement(ref HaltUpdateMovement);
            if (HaltUpdateMovement)
                return;

            float MoveScaleMultiplier = 1;
            GetMoveScaleMultiplier(ref MoveScaleMultiplier);

            float RotationScaleMultiplier = 1;
            GetRotationScaleMultiplier(ref RotationScaleMultiplier);

            bool SkipMouseRotation = false;
            GetSkipMouseRotation(ref SkipMouseRotation);

            float MoveScale = 1.0f;
            // No positional movement if we are in the air
            if (!Controller.isGrounded)
                MoveScale = 0.0f;

            MoveScale *= Time.deltaTime * SIMULATIONRATE;



            Quaternion playerDirection = ((HmdRotatesY) ? CameraRig.centerEyeAnchor.rotation : transform.rotation);
            //remove any pitch + yaw components
            playerDirection = Quaternion.Euler(0, playerDirection.eulerAngles.y, 0);

            Vector3 euler = transform.rotation.eulerAngles;

            Vector2 touchDir = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);

            bool stepLeft = false;
            bool stepRight = false;
            stepLeft = OVRInput.GetDown(OVRInput.Button.PrimaryShoulder) || Input.GetKeyDown(KeyCode.Q);
            stepRight = OVRInput.GetDown(OVRInput.Button.SecondaryShoulder) || Input.GetKeyDown(KeyCode.E);

            OVRInput.Controller activeController = OVRInput.GetActiveController();
            if ((activeController == OVRInput.Controller.Touchpad)
                || (activeController == OVRInput.Controller.Remote))
            {
                stepLeft |= OVRInput.GetDown(OVRInput.Button.DpadLeft);
                stepRight |= OVRInput.GetDown(OVRInput.Button.DpadRight);
            }
            else if ((activeController == OVRInput.Controller.LTrackedRemote)
                || (activeController == OVRInput.Controller.RTrackedRemote))
            {
                if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
                {
                    if ((touchDir.magnitude > 0.3f)
                        && (Mathf.Abs(touchDir.x) > Mathf.Abs(touchDir.y)))
                    {
                        stepLeft |= (touchDir.x < 0.0f);
                        stepRight |= (touchDir.x > 0.0f);
                    }
                }
            }

            float rotateInfluence = RotationAmount * RotationScaleMultiplier * m_SimulationRate;

            if (!SkipMouseRotation)
            {
                m_PendingRotation += Input.GetAxis(MOUSEX) * rotateInfluence * 3.25f;
            }

            float rightAxisX = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x;
            if (Mathf.Abs(rightAxisX) < m_AxisDeadZone)
                rightAxisX = 0;

            m_PendingRotation += rightAxisX * rotateInfluence;


            if (SnapRotation)
            {
                if (Mathf.Abs(m_PendingRotation) > RotationRatchet)
                {
                    if (m_PendingRotation > 0)
                        stepRight = true;
                    else
                        stepLeft = true;
                    m_PendingRotation -= Mathf.Sign(m_PendingRotation) * RotationRatchet;
                }
            }
            else
            {
                euler.y += m_PendingRotation;
                m_PendingRotation = 0;
            }



            if (m_RotationAnimation > 0f && m_Animating)
            {
                float speed = Mathf.Max(m_RotationAnimation, 3);

                float diff = AngleDifference(m_TargetYaw, euler.y);

                euler.y += Mathf.Sign(diff) * speed * Time.deltaTime;

                if ((AngleDifference(m_TargetYaw, euler.y) < 0) != (diff < 0))
                {
                    m_Animating = false;
                    euler.y = m_TargetYaw;
                }
            }
            if (stepLeft ^ stepRight)
            {
                float change = stepRight ? RotationRatchet : -RotationRatchet;

                if (m_RotationAnimation > 0)
                {
                    m_TargetYaw = (euler.y + change) % 360;
                    m_Animating = true;
                    // animationStartAngle = euler.y;
                }
                else
                {
                    euler.y += change;
                }
            }

            float moveInfluence = Acceleration * 0.1f * MoveScale * MoveScaleMultiplier * m_SimulationRate;

            // Run!
            if (OVRInput.Get(m_RunButton) || OVRInput.Get(m_AlternateRunButton) || Input.GetKey(KeyCode.LeftShift))
            {
                moveInfluence *= 2.0f;
            }


            float leftAxisX = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).x;
            float leftAxisY = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y;

            if (activeController == OVRInput.Controller.Touchpad)
            {
                leftAxisY = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y;
            }
            else if ((activeController == OVRInput.Controller.LTrackedRemote)
                || (activeController == OVRInput.Controller.RTrackedRemote))
            {
                if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                {
                    if ((touchDir.magnitude > 0.3f)
                        && (Mathf.Abs(touchDir.y) > Mathf.Abs(touchDir.x)))
                    {
                        leftAxisY = (touchDir.y > 0.0f) ? 1 : -1;
                    }
                }
            }

            if (Mathf.Abs(leftAxisX) < m_AxisDeadZone)
            {
                leftAxisX = 0;
            }
            if (Mathf.Abs(leftAxisY) < m_AxisDeadZone)
            {
                leftAxisY = 0;
            }

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                leftAxisY = 1;
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                leftAxisX = -1;
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                leftAxisX = 1;
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                leftAxisY = -1;
            }

            if (activeController == OVRInput.Controller.Remote)
            {
                if (OVRInput.Get(OVRInput.Button.DpadUp))
                {
                    leftAxisY = 1;
                }
                else if (OVRInput.Get(OVRInput.Button.DpadDown))
                {
                    leftAxisY = -1;
                }
            }

            if (leftAxisY > 0.0f)
            {
                MoveControl += leftAxisY * (playerDirection * (Vector3.forward * moveInfluence));
            }

            if (leftAxisY < 0.0f)
            {
                MoveControl += Mathf.Abs(leftAxisY) * (playerDirection * (Vector3.back * moveInfluence));
            }

            if (leftAxisX < 0.0f)
            {
                MoveControl += Mathf.Abs(leftAxisX) * (playerDirection * (Vector3.left * moveInfluence));
            }

            if (leftAxisX > 0.0f)
            {
                MoveControl += leftAxisX * (playerDirection * (Vector3.right * moveInfluence));
            }

            transform.rotation = Quaternion.Euler(euler);
        }
    }
}
