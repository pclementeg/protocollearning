﻿/**
 * The Class ProtocolProperties
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/Protocol Properties")]
    public class ProtocolProperties : MonoBehaviour
    {
        [SerializeField]
        private Protocol m_Protocol;
        [SerializeField]
        private Transform m_StartingTransform;


        public bool VerifyProtocol()
        {
            bool canBeDone = false;
            bool anyActionPossible = false;
            for (int stepIndex = 0; stepIndex < m_Protocol.m_Steps.Length; ++stepIndex)
            {
                anyActionPossible = false;
                for (int actionIndex = 0; actionIndex < m_Protocol.m_Steps[stepIndex].m_SuccessActions.Length; ++actionIndex)
                {
                    anyActionPossible |= m_Protocol.m_Steps[stepIndex].m_SuccessActions[actionIndex].VerifyAction();
                }
                canBeDone &= anyActionPossible;
            }

            return canBeDone;
        }

        public Transform GetStartingTransform()
        {
            return m_StartingTransform;
        }

        public Protocol GetProtocol()
        {
            return m_Protocol;
        }

    }
}
