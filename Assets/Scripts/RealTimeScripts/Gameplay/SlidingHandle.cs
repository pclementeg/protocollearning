﻿/**
 * The Class SlidingHandle
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System.Collections;
using UnityEngine;

namespace ProtocolLearning
{
    [AddComponentMenu("Sliding Handle")]
    public class SlidingHandle : Handle
    {
        [SerializeField]
        private SlidingHandle m_PairHandle;
        public enum Direction { PositiveX, PositiveZ, NegativeX, NegativeZ };
        [SerializeField]
        private Direction m_OpenDirection = Direction.PositiveZ;
        private float m_ClosePosition;
        [SerializeField]
        private float m_OpenOffset = 1f;
        [SerializeField]
        [Range(5f, 180f)]
        private float m_EpsilonCloseOffset = 0.1f;
        private float m_OpenPosition = 0f;
        private float m_EpsilonClosePosition = 0f;
        private bool m_isStarted = false;


        private new void Start()
        {
            if (!m_isStarted)
            {
                base.Start();
                m_CloseTransform = m_HandlePivot.parent;
                switch (m_OpenDirection)
                {
                    case Direction.PositiveZ:
                        m_ClosePosition = m_HandlePivot.localPosition.z;
                        m_OpenPosition = m_ClosePosition + m_OpenOffset;
                        m_EpsilonClosePosition = m_ClosePosition + m_EpsilonCloseOffset;
                        break;
                    case Direction.PositiveX:
                        m_ClosePosition = m_HandlePivot.localPosition.x;
                        m_OpenPosition = m_ClosePosition + m_OpenOffset;
                        m_EpsilonClosePosition = m_ClosePosition + m_EpsilonCloseOffset;
                        break;
                    case Direction.NegativeZ:
                        m_ClosePosition = m_HandlePivot.localPosition.z;
                        m_OpenPosition = m_ClosePosition - m_OpenOffset;
                        m_EpsilonClosePosition = m_ClosePosition - m_EpsilonCloseOffset;
                        break;
                    case Direction.NegativeX:
                        m_ClosePosition = m_HandlePivot.localPosition.x;
                        m_OpenPosition = m_ClosePosition - m_OpenOffset;
                        m_EpsilonClosePosition = m_ClosePosition - m_EpsilonCloseOffset;
                        break;
                }
                m_HandleOffsetPosition = this.transform.localPosition;
                m_HandleOffsetRotation = this.transform.localRotation;
                m_isStarted = true;
            }
        }

        public override void Initialize(OpenCloseAction.State startingState)
        {
            if (!m_isStarted)
            {
                Start();
            }
            if (startingState == OpenCloseAction.State.Open)
            {
                switch (m_OpenDirection)
                {
                    case Direction.PositiveZ:
                        m_HandlePivot.SetLocalZ(m_ClosePosition + m_OpenOffset);
                        break;
                    case Direction.PositiveX:
                        m_HandlePivot.SetLocalX(m_ClosePosition + m_OpenOffset);
                        break;
                    case Direction.NegativeZ:
                        m_HandlePivot.SetLocalZ(m_ClosePosition - m_OpenOffset);
                        break;
                    case Direction.NegativeX:
                        m_HandlePivot.SetLocalX(m_ClosePosition - m_OpenOffset);
                        break;
                }
                m_State = OpenCloseAction.State.Open;
            }
        }

        protected override void DoMovement()
        {
            switch (m_OpenDirection)
            {
                case Direction.PositiveZ:
                    StartCoroutine(MovePositiveZ());
                    break;
                case Direction.PositiveX:
                    StartCoroutine(MovePositiveX());
                    break;
                case Direction.NegativeZ:
                    StartCoroutine(MoveNegativeZ());
                    break;
                case Direction.NegativeX:
                    StartCoroutine(MoveNegativeX());
                    break;
            }
        }

        private void SynMoved(float currentOffset)
        {
            switch (m_OpenDirection)
            {
                case Direction.PositiveZ:
                    m_HandlePivot.SetLocalZ(m_ClosePosition + currentOffset);
                    break;
                case Direction.PositiveX:
                    m_HandlePivot.SetLocalX(m_ClosePosition + currentOffset);
                    break;
                case Direction.NegativeZ:
                    m_HandlePivot.SetLocalZ(m_ClosePosition - currentOffset);
                    break;
                case Direction.NegativeX:
                    m_HandlePivot.SetLocalX(m_ClosePosition - currentOffset);
                    break;
            }
            EndMovement();
        }

        IEnumerator MovePositiveZ()
        {
            float currentPosition = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_CloseTransform.InverseTransformPoint(m_grabbedBy.transform.position).z;
                currentPosition = Mathf.Max(m_ClosePosition, Mathf.Min(m_OpenPosition, currentPosition));
                m_HandlePivot.SetLocalZ(currentPosition);
                if (m_PairHandle)
                {
                    m_PairHandle.SynMoved(currentPosition - m_ClosePosition);
                }
                m_State = currentPosition > m_EpsilonClosePosition ? OpenCloseAction.State.Open : OpenCloseAction.State.Close;
                yield return new WaitForEndOfFrame();
            }
            EndMovement();
        }

        IEnumerator MovePositiveX()
        {
            float currentPosition = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_CloseTransform.InverseTransformPoint(m_grabbedBy.transform.position).x;
                currentPosition = Mathf.Max(m_ClosePosition, Mathf.Min(m_OpenPosition, currentPosition));
                m_HandlePivot.SetLocalX(currentPosition);
                if (m_PairHandle)
                {
                    m_PairHandle.SynMoved(currentPosition - m_ClosePosition);
                }
                m_State = currentPosition > m_EpsilonClosePosition ? OpenCloseAction.State.Open : OpenCloseAction.State.Close;
                yield return new WaitForEndOfFrame();
            }
            EndMovement();
        }

        IEnumerator MoveNegativeZ()
        {
            float currentPosition = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_CloseTransform.InverseTransformPoint(m_grabbedBy.transform.position).z;
                currentPosition = Mathf.Min(m_ClosePosition, Mathf.Max(m_OpenPosition, currentPosition));
                m_HandlePivot.SetLocalZ(currentPosition);
                if (m_PairHandle)
                {
                    m_PairHandle.SynMoved(m_ClosePosition - currentPosition);
                }
                m_State = currentPosition < m_EpsilonClosePosition ? OpenCloseAction.State.Open : OpenCloseAction.State.Close;
                yield return new WaitForEndOfFrame();
            }
            EndMovement();
        }

        IEnumerator MoveNegativeX()
        {
            float currentPosition = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_CloseTransform.InverseTransformPoint(m_grabbedBy.transform.position).x;
                currentPosition = Mathf.Min(m_ClosePosition, Mathf.Max(m_OpenPosition, currentPosition));
                m_HandlePivot.SetLocalX(currentPosition);
                if (m_PairHandle)
                {
                    m_PairHandle.SynMoved(m_ClosePosition - currentPosition);
                }
                m_State = currentPosition < m_EpsilonClosePosition ? OpenCloseAction.State.Open : OpenCloseAction.State.Close;
                yield return new WaitForEndOfFrame();
            }
            EndMovement();
        }
    }
}
