﻿/**
 * The Class SpinHandle
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System.Collections;
using UnityEngine;

namespace ProtocolLearning
{
    [AddComponentMenu("Spin Handle")]
    public class SpinHandle : Handle
    {
        public enum Direction { Positive, Negative };
        [SerializeField]
        protected Direction m_OpenDirection = Direction.Positive;
        private Quaternion m_CloseRotation;
        [SerializeField]
        [Range(5f, 180f)]
        private float m_OpenOffset = 90f;
        [SerializeField]
        [Range(5f, 180f)]
        private float m_EpsilonCloseRotation = 10f;
        private Quaternion m_OpenRotation;
        private float m_OpenRotationY;
        private float m_RotationOffset = 0f;
        private bool m_isStarted = false;

        private new void Start()
        {
            if (!m_isStarted)
            {
                base.Start();
                if (!m_HandlePivot)
                {
                    m_HandlePivot = transform.parent;
                }
                m_CloseTransform = m_HandlePivot;
                m_CloseRotation = m_HandlePivot.rotation;
                switch (m_OpenDirection)
                {
                    case Direction.Positive:
                        m_OpenRotation = Quaternion.Euler(m_CloseRotation.eulerAngles + Vector3.up * m_OpenOffset);
                        m_OpenRotationY = m_CloseRotation.eulerAngles.y + m_OpenOffset;
                        m_RotationOffset = m_OpenRotationY - m_CloseRotation.eulerAngles.y;
                        break;
                    case Direction.Negative:
                        m_OpenRotation = Quaternion.Euler(m_CloseRotation.eulerAngles - Vector3.up * m_OpenOffset);
                        m_OpenRotationY = m_CloseRotation.eulerAngles.y - m_OpenOffset;
                        m_RotationOffset = m_CloseRotation.eulerAngles.y - m_OpenRotationY;
                        break;
                }
                m_HandleOffsetPosition = this.transform.localPosition;
                m_HandleOffsetRotation = this.transform.localRotation;
                m_isStarted = true;
            }
        }

        public override void Initialize(OpenCloseAction.State startingState)
        {
            if (!m_isStarted)
            {
                Start();
            }
            if (startingState == OpenCloseAction.State.Open)
            {
                m_HandlePivot.RotateY(m_OpenDirection == Direction.Positive ? m_OpenOffset : -m_OpenOffset);
                m_State = OpenCloseAction.State.Open;
            }
        }

        protected override void DoMovement()
        {
            switch (m_OpenDirection)
            {
                case Direction.Positive:
                    StartCoroutine(MovePositive());
                    break;
                case Direction.Negative:
                    StartCoroutine(MoveNegative());
                    break;
            }
        }

        override protected IEnumerator MovePositive()
        {
            Vector3 currentPosition;
            Quaternion currentRotation = Quaternion.identity;
            float yRotation = 0f;
            float fixedRotation = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_grabbedBy.transform.position;
                currentPosition.y = m_HandlePivot.position.y;
                currentRotation = Quaternion.LookRotation(m_HandlePivot.position - currentPosition, Vector3.up);
                yRotation = currentRotation.eulerAngles.y;
                if (yRotation > m_OpenRotationY)
                {
                    yRotation -= 360f;
                }
                fixedRotation = Mathf.Max(m_CloseRotation.eulerAngles.y, Mathf.Min(yRotation + m_RotationOffset, m_OpenRotationY));
                currentRotation.eulerAngles = new Vector3(currentRotation.eulerAngles.x, fixedRotation, currentRotation.eulerAngles.z);
                m_HandlePivot.rotation = currentRotation;
                if (currentRotation.eulerAngles.y - m_CloseRotation.eulerAngles.y > m_EpsilonCloseRotation)
                {
                    m_State = OpenCloseAction.State.Open;
                }
                else
                {
                    m_State = OpenCloseAction.State.Close;
                }
                yield return new WaitForEndOfFrame();
            }

            EndMovement();
        }

        override protected IEnumerator MoveNegative()
        {
            Vector3 currentPosition;
            Quaternion currentRotation = Quaternion.identity;
            float yRotation = 0f;
            while (m_grabbedBy != null)
            {
                currentPosition = m_grabbedBy.transform.position;
                currentPosition.y = m_HandlePivot.position.y;
                currentRotation = Quaternion.LookRotation(m_HandlePivot.position - currentPosition, Vector3.up);
                yRotation = currentRotation.eulerAngles.y;

                if (yRotation < m_OpenRotationY)
                {
                    yRotation += 180f;
                    currentRotation.eulerAngles = new Vector3(currentRotation.eulerAngles.x,
                Mathf.Min(m_CloseRotation.eulerAngles.y, Mathf.Max(yRotation - m_RotationOffset, m_OpenRotationY)),
                    currentRotation.eulerAngles.z);
                }
                else
                {
                    currentRotation.eulerAngles = new Vector3(currentRotation.eulerAngles.x,
               Mathf.Min(m_CloseRotation.eulerAngles.y, Mathf.Max(yRotation + m_RotationOffset, m_OpenRotationY)),
                   currentRotation.eulerAngles.z);
                }

                m_HandlePivot.rotation = currentRotation;
                if (m_CloseRotation.eulerAngles.y - m_EpsilonCloseRotation > currentRotation.eulerAngles.y)
                {
                    m_State = OpenCloseAction.State.Open;
                }
                else
                {
                    m_State = OpenCloseAction.State.Close;
                }
                yield return new WaitForEndOfFrame();
            }

            EndMovement();
        }
    }
}
