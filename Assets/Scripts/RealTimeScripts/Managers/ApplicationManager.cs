﻿/**
 * The Class ApplicationManager
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace ProtocolLearning
{
    [AddComponentMenu("ProtocolLearning/ApplicationManager")]
    public class ApplicationManager : Singleton<ApplicationManager>
    {
        public static bool m_OnMenu = true;

        private void Awake()
        {
            AwakeSingleton(this, false);
        }

        void Start()
        {
            m_OnMenu = true;
            if (XRDevice.isPresent)
            {
                Application.targetFrameRate = Mathf.RoundToInt(XRDevice.refreshRate);
                Debug.Log("Application.targetFrameRate " + Application.targetFrameRate);
            }
            else
            {
                Application.targetFrameRate = -1;
            }
        }

        public static void QuitApplication()
        {
            //if (Input.GetKeyDown(KeyCode.Escape))
            //{
#if !UNITY_EDITOR
            //Application.Quit();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
          //  }
        }
    }
}
