﻿/**
 * The Class MenuManager
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using VRStandardAssets.Utils;
using ProtocolLearningData;
using System.Linq;

namespace ProtocolLearning
{
    [AddComponentMenu("ProtocolLearning/MenuManager")]
    public class MenuManager : Singleton<MenuManager>
    {
        [Tooltip("Active Monitor UI without HMD")]
        [SerializeField]
        private bool m_EnableMonitorUI = false;

        [SerializeField]
        private Reticle m_Reticle;
        [SerializeField]
        private SelectionRadial m_Radial;
        [SerializeField]
        private UsersManager m_UsersManager;
        [SerializeField]
        private ProtocolManager m_ProtocolManager;

        //VR UI
        [SerializeField]
        private GameObject m_VRMenu;
        [SerializeField]
        private GameObject m_VRCamera;
        [SerializeField]
        private GameObject m_VRMenuCamera;
        [SerializeField]
        private OVRInputModule m_OVRInputModule;
        public GameObject m_VRChooseMultiuserModeGUI;
        public GameObject m_VRProtocolSelectionGUI;
        public GameObject m_ContinueWithoutHMDGUI;
        //Monitor UI
        [SerializeField]
        private GameObject m_MonitorMenu;
        [SerializeField]
        private StandaloneInputModule m_InputModule;
        [SerializeField]
        private GameObject m_ChooseMultiuserModeGUI;
        [SerializeField]
        private GameObject m_LogInCanvasGUI;
        [SerializeField]
        private GameObject m_SupervisorCreationGUI;
        [SerializeField]
        private GameObject m_SupervisorGUI;
        [SerializeField]
        private GameObject m_StudentGUI;
        [SerializeField]
        private GameObject m_ContinueWithHMDGUI;
        [SerializeField]
        private GameObject m_MonitorProtocolPause;

        //Running Protocol UI
        [SerializeField]
        private GameObject m_ProtocolMenu;
        [SerializeField]
        private GameObject m_ProtocolIntroduction;
        [SerializeField]
        private GameObject m_StepTutorial;
        [SerializeField]
        private GameObject m_ProtocolEnd;
        [SerializeField]
        private GameObject m_StepTest;
        [SerializeField]
        private GameObject m_ProtocolPause;
        [SerializeField]
        private DisplayTextConsequence m_DisplayTextConsequence;

        private GameObject m_CurrentVRMenu;
        private GameObject m_CurrentMonitorMenu;
        private GameObject m_CurrentProtocolMenu;

        [SerializeField]
        private PlayerController m_PlayerController;
        private Transform m_PlayerTransform;

        private const string TRACKINGSPACE = "TrackingSpace";
        private const string OVRCAMERARIG = "OVRCameraRig";
        private const string CENTEREYEANCHOR = "CenterEyeAnchor";

        // OVR SDK Objects
        static public OVRCameraRig m_CameraRig
        {
            get
            {
                return GameObject.Find(OVRCAMERARIG).GetComponent<OVRCameraRig>();
            }
        }

        public OVRManager m_OVRManager { get; private set; }
        public GameObject m_OVRLeftCamera { get; private set; }
        public GameObject m_OVRRightCamera { get; private set; }
        public Transform m_OVRCenterEyeTransform { get; private set; }

        [SerializeField]
        private Canvas m_VRCanvasObject;

        // Input module
        [SerializeField]
        private EventSystem m_EventSystem;


        private void Awake()
        {
            AwakeSingleton(this, false);
            OnAwakeOrLevelLoad();
        }

        private void OnEnable()
        {
            if (m_EnableMonitorUI)
            {
                OVRManager.HMDMounted += HMDMounted;
                OVRManager.HMDUnmounted += HMDUnmounted;
            }
            m_ProtocolManager.OnPause += OnPause;
            m_ProtocolManager.OnNextStep += OnNextStep;
            m_ProtocolManager.OnStepSuccess += OnStepSuccess;
            m_ProtocolManager.OnStepFail += OnStepFail;
            m_ProtocolManager.OnProtocolEnd += OnProtocolEnd;
        }


        private void OnDisable()
        {
            if (m_EnableMonitorUI)
            {
                OVRManager.HMDMounted -= HMDMounted;
                OVRManager.HMDUnmounted -= HMDUnmounted;
            }
            m_ProtocolManager.OnPause -= OnPause;
            m_ProtocolManager.OnNextStep -= OnNextStep;
            m_ProtocolManager.OnStepSuccess -= OnStepSuccess;
            m_ProtocolManager.OnStepFail -= OnStepFail;
            m_ProtocolManager.OnProtocolEnd -= OnProtocolEnd;
        }

        void OnAwakeOrLevelLoad()
        {
            if (instance != this)
                return;

            OVRManager.display.RecenterPose();

            AssignCameraRig();
        }

        private void Start()
        {
            m_PlayerTransform = m_PlayerController.transform;
            m_PlayerController.EnableLinearMovement = false;
            m_ContinueWithoutHMDGUI.SetActive(false);
            m_VRChooseMultiuserModeGUI.SetActive(false);
            m_VRProtocolSelectionGUI.SetActive(false);
            m_CurrentVRMenu = m_VRChooseMultiuserModeGUI;

            m_LogInCanvasGUI.SetActive(false);
            m_SupervisorGUI.SetActive(false);
            m_ContinueWithHMDGUI.SetActive(false);
            m_SupervisorCreationGUI.SetActive(false);
            m_StudentGUI.SetActive(false);
            m_ProtocolPause.SetActive(false);
            m_CurrentMonitorMenu = m_ChooseMultiuserModeGUI;

            m_ProtocolMenu.SetActive(false);
            m_ProtocolIntroduction.SetActive(false);
            m_StepTutorial.SetActive(false);
            m_ProtocolEnd.SetActive(false);
            m_StepTest.SetActive(false);
            m_DisplayTextConsequence.gameObject.SetActive(false);
            m_CurrentProtocolMenu = m_ProtocolIntroduction;

            if (OVRManager.instance.isUserPresent || !m_EnableMonitorUI)
            {
                HMDMounted();
            }
            else
            {
                HMDUnmounted();
            }
        }

        public void AssignCameraRig()
        {
            FindCamera();
            // There has to be an event system for the GUI to work
            if (!m_EventSystem)
            {
                m_EventSystem = FindObjectOfType<EventSystem>();
                m_OVRInputModule = m_EventSystem.GetComponent<OVRInputModule>();
            }
            
            m_CameraRig.EnsureGameObjectIntegrity();
            m_VRCanvasObject.GetComponent<Canvas>().worldCamera = m_CameraRig.leftEyeCamera;
        }

        void FindCamera()
        {
            if (m_CameraRig)
            {
                Transform t = m_CameraRig.transform.Find(TRACKINGSPACE);
                m_OVRCenterEyeTransform = t.Find(CENTEREYEANCHOR);
            }

            m_OVRManager = FindObjectOfType<OVRManager>();
        }

        private IEnumerator LoadVRGUI(GameObject parent)
        {
            m_CurrentVRMenu = parent;
            m_CurrentVRMenu.SetActive(true);
            m_Reticle.Show();
            m_Radial.Hide();
            
            UIFader uiFader = parent.GetComponent<UIFader>();
            if (uiFader)
            {
                yield return StartCoroutine(uiFader.FadeIn());
            }

        }

        private IEnumerator LoadVRProtocolGUI(GameObject parent)
        {
            m_CurrentProtocolMenu = parent;
            m_CurrentProtocolMenu.SetActive(true);
            m_Reticle.Show();
            m_Radial.Hide();

            UIFader uiFader = parent.GetComponent<UIFader>();
            if (uiFader)
            {
                yield return StartCoroutine(uiFader.FadeIn());
            }
            UIMovement uiMovement = m_CurrentProtocolMenu.GetComponentInChildren<UIMovement>();
            if (uiMovement)
            {
                uiMovement.enabled = true;
            }
        }

        private void CheckCurrentUser()
        {
            if (!m_UsersManager)
            {
                m_UsersManager = UsersManager.Instance;
            }

            //Oculus Menu
            if (OVRManager.instance.isUserPresent || !m_EnableMonitorUI)
            {
                m_CurrentVRMenu.SetActive(false);
                if (m_UsersManager.m_CurrentUser != null)
                {
                    //doesnt need to select a user because is one user always
                    StartCoroutine(LoadVRGUI(m_VRProtocolSelectionGUI));
                }
                else if (m_UsersManager.IsMultiuser())
                {
                   StartCoroutine(LoadVRGUI(m_ContinueWithoutHMDGUI));
                }
                else
                {
                    //Select user type.
                    StartCoroutine(LoadVRGUI(m_VRChooseMultiuserModeGUI));
                }
            }
            else //Monitor Menu
            {
                m_CurrentMonitorMenu.SetActive(false);
                if (m_UsersManager.m_CurrentUser != null)
                {
                    //doesnt need to select a user because is one user always
                    m_CurrentMonitorMenu = m_ContinueWithHMDGUI;
                }
                else if (m_UsersManager.IsMultiuser())
                {
                    if (m_UsersManager.HasSupervisorAccount())
                    {
                        m_CurrentMonitorMenu = m_LogInCanvasGUI;
                    }
                    else
                    {
                        m_CurrentMonitorMenu = m_SupervisorCreationGUI;
                    }
                }
                else
                {
                    //Select user type.
                    m_CurrentMonitorMenu = m_ChooseMultiuserModeGUI;
                }
                m_CurrentMonitorMenu.SetActive(true);
            }
        }

        public void CheckProtocolState()
        {
            if (!m_ProtocolManager)
            {
                m_ProtocolManager = ProtocolManager.Instance;
            }

            //Oculus Menu
            if (OVRManager.instance.isUserPresent || !m_EnableMonitorUI)
            {
                UIMovement uiMovement = m_CurrentProtocolMenu.GetComponentInChildren<UIMovement>();
                if (uiMovement)
                {
                    uiMovement.enabled = false;
                }
                m_CurrentProtocolMenu.SetActive(false);
                switch (m_ProtocolManager.m_ProtocolState)
                {
                    case ProtocolStage.Introduction:
                        StartCoroutine(LoadVRProtocolGUI(m_ProtocolIntroduction));
                        break;
                    case ProtocolStage.Paused:
                        StartCoroutine(LoadVRProtocolGUI(m_ProtocolPause));
                        break;
                    case ProtocolStage.Ending:
                        StartCoroutine(LoadVRProtocolGUI(m_ProtocolEnd));
                        break;
                    case ProtocolStage.Progress:
                        if (m_ProtocolManager.m_isTestMode)
                        {
                            StartCoroutine(LoadVRProtocolGUI(m_StepTutorial));
                        }
                        else
                        {
                            StartCoroutine(LoadVRProtocolGUI(m_StepTest));
                        }
                        break;
                }
            }
            else //Monitor Menu
            {
                m_CurrentMonitorMenu.SetActive(false);
                m_CurrentMonitorMenu = m_MonitorProtocolPause;
                m_CurrentMonitorMenu.SetActive(true);
            }
        }

        public void MultiuserSelection(bool ismultiuser)
        {
#if LOGGER
            Debug.Log("Button Multiuser " + ismultiuser );
#endif
            m_UsersManager.SetAppAsMultiuser(ismultiuser);
            CheckCurrentUser();
        }

        public IEnumerator FadeOutCamera()
        {
            //StartCoroutine(m_VRCamera.GetComponent<VRCameraFade>().BeginFadeOut(true));
            yield return StartCoroutine(m_VRMenuCamera.GetComponent<VRCameraFade>().BeginFadeOut(true));
            
        }

        public IEnumerator FadeInCamera()
        {
            //StartCoroutine(m_VRCamera.GetComponent<VRCameraFade>().BeginFadeIn(true));
            yield return StartCoroutine(m_VRMenuCamera.GetComponent<VRCameraFade>().BeginFadeIn(true));
        }

        private void HMDMounted()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            m_VRMenu.SetActive(!m_ProtocolManager.m_IsRunningProtocol);
            m_ProtocolMenu.SetActive(m_ProtocolManager.m_IsRunningProtocol);
            m_MonitorMenu.SetActive(false);
            m_OVRInputModule.enabled = true;
            m_InputModule.enabled = false;
            m_VRCamera.SetActive(true);

            if (m_ProtocolManager.m_IsRunningProtocol)
            {
                CheckProtocolState();
                m_PlayerController.EnableLinearMovement = true;
            }
            else
            {
                CheckCurrentUser();
            }
            StartCoroutine(FadeInCamera());
#if LOGGER
            Debug.Log("HMDMounted");
#endif
        }

        private void HMDUnmounted()
        {
            m_PlayerController.EnableLinearMovement = false;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            m_VRMenu.SetActive(false);
            m_ProtocolMenu.SetActive(false);
            m_MonitorMenu.SetActive(true);
            m_OVRInputModule.enabled = false;
            m_InputModule.enabled = true;
            m_VRCamera.SetActive(false);
            if (m_ProtocolManager.m_IsRunningProtocol)
            {
                CheckProtocolState();
            }
            else
            {
                CheckCurrentUser(); 
            }
            
#if LOGGER
            Debug.Log("HMDUnmounted");
#endif
        }

        public void SetProtocolGUI(bool activeProtocolGUI)
        {
            m_ProtocolMenu.SetActive(activeProtocolGUI);
            m_VRMenu.SetActive(!activeProtocolGUI);
            
            CheckProtocolState();
        }

        public void NextTutorialStep()
        {
            m_CurrentProtocolMenu.SetActive(false);
            if (m_ProtocolManager.LoadNextStep())
            {
                m_CurrentProtocolMenu = m_StepTutorial;
            }
            else
            {
                m_CurrentProtocolMenu = m_ProtocolEnd;
            }
            m_CurrentProtocolMenu.SetActive(true);
        }

        public void ShowConsequenceText(string textTitle, Color textTitleColor, string textBody, Color textBodyColor, float showingTime)
        {
            UIMovement uiMovement = m_DisplayTextConsequence.GetComponentInChildren<UIMovement>();
            if (uiMovement)
            {
                uiMovement.enabled = true;
            }
            m_DisplayTextConsequence.gameObject.SetActive(true);
            m_DisplayTextConsequence.DisplayText(textTitle, textTitleColor, textBody, textBodyColor, showingTime);
        }

        public void HideConsequenceText()
        {
            UIMovement uiMovement = m_DisplayTextConsequence.GetComponentInChildren<UIMovement>();
            if (uiMovement)
            {
                uiMovement.enabled = false;
            }
            m_DisplayTextConsequence.gameObject.SetActive(false);
        }

        public void NextTestStep()
        {
            //m_CurrentProtocolMenu.SetActive(false);
            //if (m_ProtocolManager.NextTutorialStep())
            //{
            //    m_CurrentProtocolMenu = m_StepTutorial;
            //}
            //else
            //{
            //    m_CurrentProtocolMenu = m_ProtocolEnding;
            //}
            //m_CurrentProtocolMenu.SetActive(true);
        }

        private void OnPause()
        {
            CheckProtocolState();
        }

        private void OnNextStep()
        {
            //if (m_ProtocolManager.)
#if LOGGER
            Debug.Log("OnNextStep");
#endif
        }

        private void OnStepSuccess()
        {
            //if (m_ProtocolManager.)
#if LOGGER
            Debug.Log("OnStepSuccess");
#endif
        }

        private void OnStepFail()
        {
            //if (m_ProtocolManager.)
#if LOGGER
            Debug.Log("OnProtocolEnd");
#endif
        }

        private void OnProtocolEnd()
        {
            //if (m_ProtocolManager.)
#if LOGGER
            Debug.Log("OnProtocolEnd");
#endif
            CheckProtocolState();
        }

        public void QuitAplication()
        {
            ApplicationManager.QuitApplication();
        }

        void LateUpdate()
        {
            if (m_CurrentProtocolMenu)
            {
                m_CurrentProtocolMenu.transform.position = m_PlayerTransform.position;
                m_DisplayTextConsequence.transform.position = m_PlayerTransform.position;
            }
        }

        void Update()
        {
#if LOGGER
            if (Input.GetMouseButtonDown(0))
            {
                PointerEventData data = new PointerEventData(EventSystem.current);
                List<RaycastResult> result = new List<RaycastResult>();
                EventSystem.current.RaycastAll(data, result);
                result = result.OrderBy(x => x.distance).ToList();
                for (int i = 0; i < result.Count; i++)
                {
                    Debug.Log("result " + result[i]);
                }
                if (EventSystem.current.currentSelectedGameObject)
                Debug.Log("Button " + EventSystem.current.currentSelectedGameObject.name);
            }
#endif
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                QuitAplication();
            }

            if (!OVRManager.instance.isUserPresent && m_EnableMonitorUI)
            {
                return;
            }

            OVRInput.Controller activeController = OVRInput.GetActiveController();
            Transform activeTransform = m_CameraRig.centerEyeAnchor;

            if ((activeController == OVRInput.Controller.LTouch) || (activeController == OVRInput.Controller.LTrackedRemote))
                activeTransform = m_CameraRig.leftHandAnchor;

            if ((activeController == OVRInput.Controller.RTouch) || (activeController == OVRInput.Controller.RTrackedRemote))
                activeTransform = m_CameraRig.rightHandAnchor;

            if (activeController == OVRInput.Controller.Touch)
                activeTransform = m_CameraRig.rightHandAnchor;

            OVRGazePointer.instance.rayTransform = activeTransform;
            m_OVRInputModule.rayTransform = activeTransform;
            
        }

    }
}
