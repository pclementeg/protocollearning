﻿/**
 * The Class ProtocolManager
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ProtocolLearningData;

namespace ProtocolLearning
{
    [AddComponentMenu("ProtocolLearning/ProtocolManager")]
    public class ProtocolManager : Singleton<ProtocolManager>
    {
        [SerializeField]
        private Protocol[] m_Protocols;
        private UsersManager m_UsersManager;

        public ProtocolStage m_ProtocolState { get; private set; }
        private ProtocolStage m_LastProtocolState;
        public ProtocolEvaluation m_ProtocolEvaluation { get; private set; }
        public FailReason m_FailReason { get; private set; }

        //Running Protocol
        public bool m_isTestMode { get; private set; }
        //private Protocol 
        public bool m_IsRunningProtocol { get; private set; }
        [SerializeField]
        private PlayerController m_PlayerController;

        private ProtocolProperties m_CurrentProtocolProperties;
        private Protocol m_CurrentProtocol;
        private Step m_CurrentStep;
        private int m_CurrentProtocolIndex = -1;
        public float m_RunningTime { get; private set; }
        public bool m_isPaused { get; private set; }
        public int m_CurrentStepIndex { get; private set; }

        public event Action OnPause;
        public event Action OnNextStep;
        public event Action OnStepSuccess;
        public event Action OnStepFail;
        public event Action OnProtocolEnd;

        private Vector3 m_ResetPlayerPosition;
        private Quaternion m_ResetPlayerRotation;
        private WaitForSeconds m_WaitTime = new WaitForSeconds(1f);
        private const string PROTOCOLPROPERTY_GONAME = "ProtocolProperty";
        private DateTime m_ExecutionTime;


        private void Awake()
        {
            AwakeSingleton(this, false);
        }

        // Use this for initialization
        void Start()
        {
            m_UsersManager = UsersManager.Instance;
            m_isTestMode = false;
            m_IsRunningProtocol = false;
            m_LastProtocolState = ProtocolStage.Introduction;
            m_ProtocolState = ProtocolStage.Introduction;
            m_CurrentStepIndex = -1;
            m_ProtocolEvaluation = ProtocolEvaluation.Incomplete;
        }

        public List<string> GetProtocolNames()
        {
            List<string> names = new List<string>();
            for (int i = 0; i < m_Protocols.Length; i++)
            {
                names.Add(m_Protocols[i].name);
            }
            return names;
        }

        public void SetAsTestMode(bool isTestMode)
        {
            m_isTestMode = isTestMode;
        }

        public void PauseCurrentProtocol()
        {
            m_LastProtocolState = m_ProtocolState;
            m_ProtocolState = ProtocolStage.Paused;
        }

        public void ContinueProtocol()
        {
            m_ProtocolState = m_LastProtocolState;
        }

        
        public bool LoadNextStep()
        {
            if (m_CurrentStepIndex < m_Protocols[m_CurrentProtocolIndex].m_Steps.Length - 1)
            {
                m_ProtocolState = ProtocolStage.Progress;
                ++m_CurrentStepIndex;
                m_CurrentStep = m_CurrentProtocol.m_Steps[m_CurrentStepIndex];
                if (m_CurrentStep.m_TimeLimitToBeDone > 0f)
                {
                    StartCoroutine(StepTimeLimit(m_CurrentStep.m_TimeLimitToBeDone));
                }
                return true;
            }
            //there is no more steps
            m_ProtocolState = ProtocolStage.Ending;
            return false;
        }

        private IEnumerator StepTimeLimit(float timeLimit)
        {
            int currentStepIndex = m_CurrentStepIndex;

            while (m_CurrentStepIndex == currentStepIndex && timeLimit > 0f)
            {
                yield return m_WaitTime;
                --timeLimit;
            }
            if (timeLimit <= 0f)
            {
                m_FailReason = FailReason.RunOutOfTime;
                OnStepFail();
            }
        }

        public void StepActionDone(StepAction stepAction)
        {
            if (Array.Exists(m_CurrentStep.m_SuccessActions, element => stepAction))
            {
#if LOGGER
                Debug.Log("Success Action Done");
#endif
                if (m_isTestMode)
                {
                    OnStepSuccess();
                }
                else
                {
                    OnNextStep();

                }
            }

            if (m_isTestMode)
            {
                if (Array.Exists(m_CurrentStep.m_FailActions, element => stepAction))
                {
                    m_FailReason = FailReason.WrongAction;
#if LOGGER
                    Debug.Log("Fail Action Done");
#endif
                    OnStepFail();
                }
            }
        }

        public void EndProtocol()
        {
            m_ProtocolState = ProtocolStage.Ending;
            OnProtocolEnd();
        }

        public void GetIntroduction(ref string title, ref string body)
        {
            title = m_Protocols[m_CurrentProtocolIndex].m_ProtocolName;
            body = m_Protocols[m_CurrentProtocolIndex].m_ProtocolIntroduction;
        }

        public void GetEndingInformation(ref string title, ref string time)
        {
            title = m_Protocols[m_CurrentProtocolIndex].m_ProtocolName;
            time = m_RunningTime.ToString();
        }

        public void GetStep(ref string title, ref string body)
        {
            title = m_Protocols[m_CurrentProtocolIndex].m_Steps[m_CurrentStepIndex].m_StepTitle;
            body = m_Protocols[m_CurrentProtocolIndex].m_Steps[m_CurrentStepIndex].m_TutorialText;
        }

        public IEnumerator LoadProtocol(int index)
        {
            m_CurrentProtocolProperties = null;
            m_CurrentProtocol = null;
            m_CurrentStep = null;
            yield return SceneManager.LoadSceneAsync(m_Protocols[index].m_ProtocolScene, LoadSceneMode.Additive);
            m_RunningTime = 0f;
            GameObject currentProtocolGameObject = GameObject.Find(PROTOCOLPROPERTY_GONAME);
            if (currentProtocolGameObject)
            {
                m_CurrentProtocolProperties = currentProtocolGameObject.GetComponent<ProtocolProperties>();
                if (!m_CurrentProtocolProperties.VerifyProtocol())
                {
                    Debug.LogWarning("Protocol scenes doesn't match the needed steps");
                }
            }

            if (m_CurrentProtocolProperties)
            {
                if (m_Protocols[index] == m_CurrentProtocolProperties.GetProtocol())
                {
                    m_ExecutionTime = DateTime.Now;
                    m_CurrentProtocol = m_Protocols[index];
                    m_CurrentProtocol.m_ActiveConditions.Reset();
                    m_IsRunningProtocol = true;
                    m_CurrentProtocolIndex = index;
                    Transform startingTransform = m_CurrentProtocolProperties.GetStartingTransform();
                    m_ResetPlayerPosition = m_PlayerController.transform.position;
                    m_ResetPlayerRotation = m_PlayerController.transform.rotation;
                    m_PlayerController.transform.SetPositionAndRotation(startingTransform.position, startingTransform.rotation);
                    m_PlayerController.EnableLinearMovement = true;
                    ApplicationManager.m_OnMenu = false;
                }
                else
                {
                    Debug.LogWarning("Protocol scenes doesn't match");
                }
            }
            else
            {
                Debug.LogWarning("Scene loaded without Protocol Properties");
            }

        }

        public IEnumerator UnloadCurrentProtocol(bool saveLog)
        {
            m_UsersManager.SaveProtocolLog(m_Protocols[m_CurrentProtocolIndex].m_ProtocolName, m_RunningTime, m_ProtocolEvaluation, m_FailReason,
                m_Protocols[m_CurrentProtocolIndex].m_Steps[m_CurrentStepIndex].m_StepTitle, 
                m_CurrentStepIndex, m_CurrentStepIndex == m_Protocols[m_CurrentProtocolIndex].m_Steps.Length - 1, m_ExecutionTime);
            m_CurrentProtocolProperties = null;
            m_IsRunningProtocol = false;
            yield return SceneManager.UnloadSceneAsync(m_Protocols[m_CurrentProtocolIndex].m_ProtocolScene);
            m_PlayerController.transform.SetPositionAndRotation(m_ResetPlayerPosition, m_ResetPlayerRotation);
            m_CurrentProtocolIndex = -1;
        }

        public static bool CheckCondition(Condition requiredCondition)
        {
            Condition[] allConditions = Instance.m_CurrentProtocol.m_ActiveConditions.m_Conditions;
            Condition globalCondition = null;

            if (allConditions != null && allConditions[0] != null)
            {
                for (int i = 0; i < allConditions.Length; i++)
                {
                    if (allConditions[i].m_Hash == requiredCondition.m_Hash)
                        globalCondition = allConditions[i];
                }
            }

            if (!globalCondition)
                return false;
            return globalCondition.m_IsSatisfied == requiredCondition.m_IsSatisfied;
        }

        private void Update()
        {
            if (m_ProtocolState == ProtocolStage.Progress)
            {
                m_RunningTime += Time.deltaTime;
                if (OVRInput.GetDown(OVRInput.Button.Two)) // && OVRGazePointer.instance.hidden)
                {
                    PauseCurrentProtocol();
                }
            }

        }
    }
}
