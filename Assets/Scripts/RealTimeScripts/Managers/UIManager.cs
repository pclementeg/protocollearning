﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    [AddComponentMenu("ProtocolLearning/UIManager")]
    public class UIManager : Singleton<UIManager>
    {


        private void Awake()
        {
            AwakeSingleton(this, false);
        }

    }
}