﻿/**
 * The Class UsersManager
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using ProtocolLearningData;
using System;

namespace ProtocolLearning
{
    [AddComponentMenu("ProtocolLearning/UsersManager")]
    public class UsersManager : Singleton<UsersManager>
    {
        public UserData m_CurrentUser;
        private UsersData m_Users;
        private const string USERSDATAFILE = "/usersdatabase.dat";
        private const string SINGLEUSER = "SINGLEUSER";

        private void Awake()
        {
            AwakeSingleton(this, true);
        }

        private void OnEnable()
        {
            LoadUsers();
        }

        private void OnDisable()
        {
            if (m_Users.users.Count > 0)
            {
                SaveUsers();
            }
        }

        public bool IsMultiuser()
        {
            return m_Users.multiuser;// && m_Users.users.Count > 0;
        }

        public bool HasSupervisorAccount()
        {
            return (m_Users.users.Count > 0 && m_Users.users[0].supervisor);
        }

        private void LoadUsers()
        {
            if (File.Exists(Application.persistentDataPath + USERSDATAFILE))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream fileStream = File.Open(Application.persistentDataPath + USERSDATAFILE, FileMode.OpenOrCreate);
#if LOGGER
                Debug.Log("File " + Application.persistentDataPath + USERSDATAFILE);
#endif
                if (fileStream.CanRead)
                {
                    m_Users = (UsersData)binaryFormatter.Deserialize(fileStream);
#if LOGGER
                    Debug.Log("Users Loaded. Is Multiuser? " + m_Users.multiuser);
#endif
                    if (m_Users.multiuser)
                    {
#if LOGGER
                        for (int i = 0; i < m_Users.users.Count; i++)
                        {
                            Debug.Log(m_Users.users[i].name + " " + m_Users.users[i].password);
                        }
#endif
                        m_CurrentUser = null;
                    }
                    else if (m_Users.users.Count == 1)
                    {
#if LOGGER
                        Debug.Log(m_Users.users[0].name + " " + m_Users.users[0].password);
#endif
                        m_CurrentUser = m_Users.users[0];
                    }
                }
                else
                {
                    m_Users = new UsersData();
                    m_Users.users = new List<UserData>();
                    m_CurrentUser = null;
#if LOGGER
                    Debug.Log("File can't be readed. Reseted. ");
#endif
                }
                fileStream.Close();


            }
            else
            {
                m_Users = new UsersData();
                m_Users.users = new List<UserData>();
                m_CurrentUser = null;
#if LOGGER
                Debug.Log("Users file not found. New Database will be created in: " + Application.persistentDataPath + USERSDATAFILE);
#endif
            }

        }

        private void SaveUsers()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Create(Application.persistentDataPath + USERSDATAFILE);
            binaryFormatter.Serialize(fileStream, m_Users);
            fileStream.Close();
#if LOGGER
            Debug.Log("Users Saved");
#endif
        }

        public void SaveUser(UserData userToSave)
        {
            UserData foundUser = m_Users.users.Find(e => e.name == userToSave.name);
            if (foundUser == null)
            {
#if LOGGER
                Debug.Log("User doesn't found: " + userToSave.name + " is Added");
#endif
                m_Users.users.Add(userToSave);
            }
            else
            {
                foundUser.tests = userToSave.tests;
            }
        }

        public UserData GetUserData(string name)
        {
            UserData foundUser = m_Users.users.Find(e => e.name == name);
            if (foundUser == null)
            {
                Debug.LogError("User doesn't found: " + name);
                
            }
            return foundUser;
        }

        public bool CreateNewUser(string name, string password, bool isSupervisor)
        {
            UserData foundUser = m_Users.users.Find(e => e.name == name);
            if (foundUser != null)
            {
                return false;
            }
            else
            {
                UserData newUser = new UserData();
                newUser.name = name;
                newUser.password = password;
                newUser.supervisor = isSupervisor;
                newUser.tests = new List<TestData>();
                m_Users.users.Add(newUser);
                return true;
            }
        }

        public void SetAppAsMultiuser(bool multiuser)
        {
            m_Users.multiuser = multiuser;
            if (!multiuser)
            {
                CreateNewUser(SINGLEUSER, "", false);
                m_CurrentUser = m_Users.users[0];
            }

        }

        public void SaveProtocolLog(string protocolName, float time, ProtocolEvaluation evaluation, FailReason fail, string stepName, int indexStep, bool isfinished, DateTime date)
        {
            if (m_CurrentUser != null)
            {
                TestData testData = new TestData(protocolName, time, evaluation, fail, stepName, indexStep, isfinished, date);
                m_CurrentUser.tests.Add(testData);
            }
        }
    }
}
