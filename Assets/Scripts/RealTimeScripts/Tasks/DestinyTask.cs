﻿/**
 * The Class DestinyTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProtocolLearningData;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [AddComponentMenu("ProtocolLearning/Destiny Task")]
    public class DestinyTask : Task
    {
        private Collider m_DestinyArea;
        
        private new void Start()
        {
            base.Start();
            DestinyAction destinyAction = m_StepAction as DestinyAction;
            if (destinyAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type DestinyAction");
            }
            m_DestinyArea = GetComponent<Collider>();
        }

        private IEnumerator OnTriggerEnter(Collider other)
        {
            if (other.tag == Properties.PLAYER_TAG)
            {
                if (!m_StepAction.m_KeepActive && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
            yield return new WaitForSeconds(0.1f);
        }

        private IEnumerator OnTriggerExit(Collider other)
        {
            if (other.tag == Properties.PLAYER_TAG)
            {
                if ((m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
