﻿/**
 * The Class LeaveItemTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System.Collections;
using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [AddComponentMenu("LeaveItem Task")]
    public class LeaveItemTask : Task
    {
        private Collider m_DestinyArea;
        LeaveItemAction m_LeaveItemAction;

        private new void Start()
        {
            base.Start();
            m_LeaveItemAction = m_StepAction as LeaveItemAction;
            if (m_LeaveItemAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type LeaveItemAction");
            }
            m_DestinyArea = GetComponent<Collider>();
        }

        private IEnumerator OnTriggerEnter(Collider other)
        {
            if (other.tag == m_LeaveItemAction.m_ItemTag)
            {
                if (m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
            yield return new WaitForSeconds(0.1f);
        }

        private IEnumerator OnTriggerExit(Collider other)
        {
            if (other.tag == m_LeaveItemAction.m_ItemTag)
            {
                if ((m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
