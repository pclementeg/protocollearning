﻿/**
 * The Class LookAtTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(VRInteractiveItem))]
    [AddComponentMenu("ProtocolLearning/LookAtTask")]
    public class LookAtTask : Task
    {
        private VRInteractiveItem m_InteractiveItem;

        private new void Start()
        {
            base.Start();
            LookAtAction lookAtAction = m_StepAction as LookAtAction;
            if (lookAtAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type LookAtAction");
            }
            m_InteractiveItem = GetComponent<VRInteractiveItem>();
        }

        private void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
        }


        private void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
        }

        private void HandleOver()
        {
            if (!m_StepAction.m_KeepActive && m_StepAction.CheckAction(this.gameObject))
            {
                m_ProtocolManager.StepActionDone(m_StepAction);
                GoodConsequences();
            }
        }

        private void HandleOut()
        {
            if ((m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
            {
                m_ProtocolManager.StepActionDone(m_StepAction);
                GoodConsequences();
            }
        }
    }
}
