﻿/**
 * The Class OpenCloseTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using UnityEngine;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Handle))]
    [AddComponentMenu("OpenClose Task")]
    public class OpenCloseTask : Task
    {
        private Handle m_HandleItem;
        private OpenCloseAction m_OpenCLoseAction;

        private new void Start()
        {
            base.Start();
            m_OpenCLoseAction = m_StepAction as OpenCloseAction;
            if (m_OpenCLoseAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type OpenCloseAction");
            }
            //m_HandleItem = GetComponent<Handle>();
            if (m_OpenCLoseAction.m_InitialState != m_HandleItem.m_State)
            {
                m_HandleItem.Initialize(m_OpenCLoseAction.m_InitialState);
            }
        }

        private void OnEnable()
        {
            m_HandleItem = GetComponent<Handle>();
            m_HandleItem.OnGrabBegin += OnGrabBegin;
            m_HandleItem.OnGrabEnd += OnGrabEnd;
        }


        private void OnDisable()
        {
            m_HandleItem.OnGrabBegin -= OnGrabBegin;
            m_HandleItem.OnGrabEnd -= OnGrabEnd;
        }

        private new void React()
        {
            if (CheckConditions() && m_HandleItem.m_State == m_OpenCLoseAction.m_WantedState)
            {
                m_ProtocolManager.StepActionDone(m_StepAction);
                GoodConsequences();
            }
            else
            {
                FailConsequences();
            }
        }

        private void OnGrabBegin()
        {
            if (!m_StepAction.m_KeepActive && m_StepAction.CheckAction(this.gameObject))
            {
                //Debug.Log("OnGrabBegin " + gameObject.name);
                React();

            }
        }

        private void OnGrabEnd()
        {
            if ((m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
            {
                React();
            }
        }
    }
}
