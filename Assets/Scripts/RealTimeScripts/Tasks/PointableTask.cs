﻿/**
 * The Class PointableTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(EventTrigger))]
    [AddComponentMenu("PointableTask")]
    public class PointableTask : Task
    {
        private new void Start()
        {
            base.Start();
            PointableAction pointableAction = m_StepAction as PointableAction;
            if (pointableAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type PointableAction");
            }
            EventTrigger trigger = GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((data) => { OnPointerClickDelegate((PointerEventData)data); });
            trigger.triggers.Add(entry);
        }

        public void OnPointerClickDelegate(PointerEventData data)
        {
            //Debug.Log("OnPointerClick");
            if (!m_StepAction.m_KeepActive && m_StepAction.CheckAction(this.gameObject))
            {
                m_ProtocolManager.StepActionDone(m_StepAction);
                GoodConsequences();
            }
        }
    }
}