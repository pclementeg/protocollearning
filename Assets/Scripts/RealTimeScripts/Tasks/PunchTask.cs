﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProtocolLearningData;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [AddComponentMenu("Punch Task")]
    public class PunchTask : Task
    {
        private Collider m_PunchingArea;

        private new void Start()
        {
            base.Start();
            PunchAction punchAction = m_StepAction as PunchAction;
            if (punchAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type PunchAction");
            }
            m_PunchingArea = GetComponent<Collider>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == Properties.HAND_TAG)
            {
                OvrAvatarHand hand = other.transform.GetComponentInParent<OvrAvatarHand>();

                if (hand.m_HandController.m_HandState == HandController.HandState.PUSHING && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
        }

        private void OnCollisionExit(Collision other)
        {
            if (other.gameObject.tag == Properties.HAND_TAG)
            {
                OvrAvatarHand hand = other.transform.GetComponentInParent<OvrAvatarHand>();
                if (hand.m_HandController.m_HandState == HandController.HandState.PUSHING &&
                    (m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
        }
    }
}

