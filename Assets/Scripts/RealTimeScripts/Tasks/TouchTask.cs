﻿/**
 * The Class TouchTask
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using UnityEngine;


namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [AddComponentMenu("Touch Task")]
    public class TouchTask : Task
    {
        private Collider m_TouchingArea;
        private const string HAND_TAG = "Hand";

        private new void Start()
        {
            base.Start();
            TouchAction touchAction = m_StepAction as TouchAction;
            if (touchAction)
            {
                m_StepAction.m_Verified = true;
            }
            else
            {
                Debug.LogError("Content Action must be of type TouchAction");
            }
            m_TouchingArea = GetComponent<Collider>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == HAND_TAG)
            {
                OvrAvatarHand hand = other.transform.GetComponentInParent<OvrAvatarHand>();

                if (hand.m_HandController.m_HandState == HandController.HandState.TOUCHING && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
        }

        private void OnCollisionExit(Collision other)
        {
            if (other.gameObject.tag == HAND_TAG)
            {
                OvrAvatarHand hand = other.transform.GetComponentInParent<OvrAvatarHand>();
                if (hand.m_HandController.m_HandState == HandController.HandState.TOUCHING &&
                    (m_StepAction.m_KeepActive || (m_StepAction.m_KeepUntilAction && m_StepAction.m_KeepUntilAction.m_ActionDone)) && m_StepAction.CheckAction(this.gameObject))
                {
                    React();
                }
            }
        }
    }
}

