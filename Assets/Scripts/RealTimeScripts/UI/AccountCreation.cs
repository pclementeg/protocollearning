﻿/**
 * The Class AccountCreation
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/Account Creation")]
    public class AccountCreation : MonoBehaviour
    {
        private UsersManager m_UsersManager;
        public bool m_IsSupervisorAccount = false;
        [SerializeField]
        private Text m_TitleText;
        [SerializeField]
        private Text m_UserName;
        [SerializeField]
        private Text m_UserPassword;
        [SerializeField]
        private Text m_PasswordCheck;
        [SerializeField]
        private Text m_Warning;
        private const string SUPERVISOR_TITLE = "Cree la cuenta de supervisor";
        private const string USER_TITLE = "Cree una cuenta de usuario";

        private enum ErrorType {Name, PasswordMatch, UserExist };
        private const string NAME_ERROR = "No puede dejar el campo nombre vacio";
        private const string PASSWORD_ERROR = "Ambas contraseñas deben coincidir";
        private const string USEREXIST_ERROR = "El nombre de usuario ya existe";
        private const string CREATION_SUCCESS = "Cuenta creada con Exito";

        private void Start()
        {
            m_UsersManager = UsersManager.Instance;
            RemoveMessage();
        }

        private void OnEnable()
        {
            RemoveMessage();
        }

        private void WriteError(ErrorType errorType)
        {
            m_Warning.gameObject.SetActive(true);
            m_Warning.color = Color.red;
            switch (errorType)
            {
                case ErrorType.Name:
                    m_Warning.text = NAME_ERROR;
                    break;
                case ErrorType.PasswordMatch:
                    m_Warning.text = PASSWORD_ERROR;
                    break;
                case ErrorType.UserExist:
                    m_Warning.text = USEREXIST_ERROR;
                    break;
            }
        }

        private void WriteSuccess()
        {
            m_Warning.gameObject.SetActive(true);
            m_Warning.color = Color.green;
            m_Warning.text = CREATION_SUCCESS;

        }

        public void RemoveMessage()
        {
            m_Warning.gameObject.SetActive(false);
        }

        public void CreateAccount()
        {
            if (m_UserName.text.Length < 1)
            {
                WriteError(ErrorType.Name);
                return;
            }

            if (m_UserPassword.text != m_PasswordCheck.text)
            {
                WriteError(ErrorType.PasswordMatch);
                return;
            }

            if (m_UsersManager.CreateNewUser(m_UserName.text, m_UserPassword.text, m_IsSupervisorAccount))
            { 
                WriteSuccess();
                return;
            }
            else
            {
                WriteError(ErrorType.UserExist);
                return;
            }
        }
    }
}
