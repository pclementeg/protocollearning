﻿/**
 * The Class CallGUI
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/CallGUI")]
    public class CallGUI : MonoBehaviour
    {
        [SerializeField]
        private Text m_CallText;
        private ProtocolManager m_ProtocolManager;
        private MenuManager m_MenuManager;

        private void Start()
        {
            m_ProtocolManager = ProtocolManager.Instance;
            m_MenuManager = MenuManager.Instance;
        }

        public void InsertNumber(string number)
        {
            m_CallText.text += number;
        }

        public void DeleteNumber()
        {
            if (m_CallText.text.Length > 0)
            {
                m_CallText.text.Remove(m_CallText.text.Length - 1);
            }
        }

        public void CallNumber()
        {

        }
    }
}
