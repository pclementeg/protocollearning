﻿/**
 * The Class DisplayTextConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/DisplayTextConsequence")]
    public class DisplayTextConsequence : MonoBehaviour
    {
        [SerializeField]
        private Text m_TitleText;
        [SerializeField]
        private Text m_BodyText;
        private MenuManager m_MenuManager;

        void Start()
        {
            m_MenuManager = MenuManager.Instance;
        }

        public void DisplayText(string textTitle, Color textTitleColor, string textBody, Color textBodyColor, float showingTime)
        {
            m_TitleText.text = textTitle;
            m_TitleText.color = textTitleColor;
            m_BodyText.text = textBody;
            m_BodyText.color = textBodyColor;
            if (showingTime > 0f)
            {
                StartCoroutine(TimedHideText(showingTime));
            }
        }

        public void HideText()
        {
            //Debug.Log("HideText");
            m_MenuManager.HideConsequenceText();
        }

        private IEnumerator TimedHideText(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            HideText();
        }
    }
}
