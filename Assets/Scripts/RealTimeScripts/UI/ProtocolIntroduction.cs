﻿/**
 * The Class ProtocolIntroduction
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/Protocol Introduction")]
    public class ProtocolIntroduction : MonoBehaviour
    {
        private ProtocolManager m_ProtocolManager;
        private MenuManager m_MenuManager;
        [SerializeField]
        private Text m_TitleText;
        [SerializeField]
        private Text m_BodyText;

        private void Start()
        {
            m_ProtocolManager = ProtocolManager.Instance;
            m_MenuManager = MenuManager.Instance;
        }

        private void OnEnable()
        {
            string title = "";
            string body = "";
            if (m_ProtocolManager.m_IsRunningProtocol)
            {
                m_ProtocolManager.GetIntroduction(ref title, ref body);
            }
            m_TitleText.text = title;
            m_BodyText.text = body;
        }

        public void GoToSteps()
        {
            if (m_ProtocolManager.m_isTestMode)
            {
                m_MenuManager.NextTestStep();
            }
            else
            {
                m_MenuManager.NextTutorialStep();
            }
            
        }

        public void CancelCurrentProtocol()
        {
#if LOGGER
            Debug.Log("CancelCurrentProtocol");
#endif
            StartCoroutine(UnLoadCurrentProtocol());
        }

        private IEnumerator UnLoadCurrentProtocol()
        {
            yield return StartCoroutine(m_MenuManager.FadeOutCamera());
            yield return StartCoroutine(m_ProtocolManager.UnloadCurrentProtocol(false));
            m_MenuManager.SetProtocolGUI(false);
            
        }

    }
}
