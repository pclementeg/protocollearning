﻿/**
 * The Class ProtocolSelection
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProtocolLearning
{
    [DisallowMultipleComponent]
    [AddComponentMenu("ProtocolLearning/ProtocolSelection")]
    public class ProtocolSelection : MonoBehaviour
    {
        [SerializeField]
        private ProtocolManager m_ProtocolManager;
        [SerializeField]
        private MenuManager m_MenuManager;
        [SerializeField]
        private GameObject m_ToLeftButton;
        [SerializeField]
        private GameObject m_ToRightButton;
        [SerializeField]
        private GameObject[] m_ProtocolButtons;
        [SerializeField]
        private GameObject m_TutorialButton;
        [SerializeField]
        private GameObject m_TestButton;
        [SerializeField]
        private GameObject m_StartButton;
        [SerializeField]
        private GameObject m_ViewResultsButton;
        private int m_SelectedProtocolIndex = -1;
        private GameObject m_ProtocolButtonSelected = null;


        private int m_ListIndex = 0;
        private List<string> m_ProtocolNames = new List<string>();

        private void Start()
        {
            if (!m_ProtocolManager)
            {
                m_ProtocolManager = ProtocolManager.Instance;
            }
            if (!m_MenuManager)
            {
                m_MenuManager = MenuManager.Instance;
            }

            m_ProtocolNames = m_ProtocolManager.GetProtocolNames();

            if (m_ProtocolNames.Count > m_ProtocolButtons.Length)
            {
                m_ToLeftButton.SetActive(true);
                m_ToRightButton.SetActive(true);
            }
            else
            {
                m_ToLeftButton.SetActive(false);
                m_ToRightButton.SetActive(false);
            }

            for (int i = 0; i < m_ProtocolButtons.Length; ++i)
            {
                if (m_ProtocolNames.Count > i)
                {
                    m_ProtocolButtons[i].SetActive(true);
                    m_ProtocolButtons[i].GetComponentInChildren<Text>().text = m_ProtocolNames[i];
                }
                else
                {
                    m_ProtocolButtons[i].SetActive(false);
                }
            }
            m_ToLeftButton.GetComponent<Button>().interactable = false;
        }

        private void OnEnable()
        {
            m_StartButton.SetActive(false);
        }

        public void TutorialMode()
        {
            m_TestButton.GetComponent<Image>().color = m_TestButton.GetComponent<Button>().colors.normalColor;
            m_TutorialButton.GetComponent<Image>().color = m_TutorialButton.GetComponent<Button>().colors.pressedColor;
            m_StartButton.SetActive(true);
            m_ProtocolManager.SetAsTestMode(false);
        }

        public void TestMode()
        {
            m_TestButton.GetComponent<Image>().color = m_TestButton.GetComponent<Button>().colors.pressedColor;
            m_TutorialButton.GetComponent<Image>().color = m_TutorialButton.GetComponent<Button>().colors.normalColor;
            m_StartButton.SetActive(true);
            m_ProtocolManager.SetAsTestMode(true);
        }

        public void ProtocolButtonSelection(int index)
        {
            if (m_ProtocolButtonSelected)
            {
                m_ProtocolButtonSelected.GetComponent<Image>().color = m_ProtocolButtonSelected.GetComponent<Button>().colors.normalColor;
            }
            m_ProtocolButtons[index].GetComponent<Image>().color = m_ProtocolButtons[index].GetComponent<Button>().colors.pressedColor;
            m_ProtocolButtonSelected = m_ProtocolButtons[index];
            m_SelectedProtocolIndex = index + m_ListIndex;
        }

        public void ToRight()
        {
            m_ToLeftButton.GetComponent<Button>().interactable = true;
            m_ListIndex = Mathf.Min(m_ListIndex + m_ProtocolButtons.Length, m_ProtocolNames.Count - m_ProtocolButtons.Length);
            int nameIndex = m_ListIndex;
            if (m_ListIndex + m_ProtocolButtons.Length >= m_ProtocolNames.Count)
            {
                m_ToRightButton.GetComponent<Button>().interactable = false;
            }
            for (int i = 0; i < m_ProtocolButtons.Length; ++i)
            {
                if (m_ProtocolNames.Count > nameIndex)
                {
                    m_ProtocolButtons[i].SetActive(true);
                    m_ProtocolButtons[i].GetComponentInChildren<Text>().text = m_ProtocolNames[nameIndex];
                }
                else
                {
                    m_ProtocolButtons[i].SetActive(false);
                }
                ++nameIndex;
            }
            if (m_ProtocolButtonSelected)
            {
                m_ProtocolButtonSelected.GetComponent<Image>().color = m_ProtocolButtonSelected.GetComponent<Button>().colors.normalColor;
            }
            //m_ProtocolButtons[m_ListIndex].GetComponent<Button>().interactable = m_SelectedPictureIndex != m_PictureIndex + m_PreviewPictureIndex;
        }

        public void ToLeft()
        {
            m_ToRightButton.GetComponent<Button>().interactable = true;
            m_ListIndex = Mathf.Max(0, m_ListIndex - m_ProtocolButtons.Length);
            int nameIndex = m_ListIndex;
            if (m_ListIndex == 0)
            {
                m_ToLeftButton.GetComponent<Button>().interactable = false;
            }
            for (int i = 0; i < m_ProtocolButtons.Length; ++i)
            {
                if (m_ProtocolNames.Count > nameIndex)
                {
                    m_ProtocolButtons[i].SetActive(true);
                    m_ProtocolButtons[i].GetComponentInChildren<Text>().text = m_ProtocolNames[nameIndex];
                }
                else
                {
                    m_ProtocolButtons[i].SetActive(false);
                }
                ++nameIndex;
            }
            //m_ProtocolButtons[m_ListIndex].GetComponent<Button>().interactable = m_SelectedPictureIndex != m_PictureIndex + m_PreviewPictureIndex;
        }

        public void StartSelectedProtocol()
        {
#if LOGGER
            Debug.Log("SelectedProtocol " + m_ProtocolNames[m_SelectedProtocolIndex]);
#endif
            StartCoroutine(LoadSelectedProtocol());
        }

        private IEnumerator LoadSelectedProtocol()
        {
            yield return StartCoroutine(m_MenuManager.FadeOutCamera());
            yield return StartCoroutine(m_ProtocolManager.LoadProtocol(m_SelectedProtocolIndex));
            m_MenuManager.SetProtocolGUI(true);
            m_MenuManager.FadeInCamera();
        }
    }
}
