﻿using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/ActiveConditions")]
    public class ActiveConditions : ScriptableObject
    {
        public Condition[] m_Conditions;

        public void Reset()
        {
            if (m_Conditions == null)
                return;

            for (int i = 0; i < m_Conditions.Length; i++)
            {
                m_Conditions[i].m_Hash = Animator.StringToHash(m_Conditions[i].m_Description);
                m_Conditions[i].m_IsSatisfied = false;
            }
        }
    }
}