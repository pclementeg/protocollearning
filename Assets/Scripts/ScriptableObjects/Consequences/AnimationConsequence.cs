﻿/**
 * The Class AnimationConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using UnityEngine;

namespace ProtocolLearning
{
    //[CreateAssetMenu(menuName = "ProtocolLearning/Consequences/AnimationConsequence")]
    public class AnimationConsequence : DelayedConsequence
    {
        public Animator m_Animator;
        public string m_Trigger;

        protected override void SpecificInitialization(GameObject gameObject)
        {
            m_Animator = gameObject.GetComponent<Animator>();
            if (!m_Animator)
            {
                m_Animator = gameObject.AddComponent<Animator>();
            }
        }

        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_Animator = gameObject.GetComponent<Animator>();
            m_Animator.SetTrigger(Animator.StringToHash(m_Trigger));
        }
    }
}