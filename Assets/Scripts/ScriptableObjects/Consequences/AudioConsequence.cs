﻿/**
 * The Class AudioConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Consequences/AudioConsequence")]
    public class AudioConsequence : Consequence
    {
        public AudioSource m_AudioSource;
        public AudioClip m_AudioClip;
        public float m_Delay;

        protected override void SpecificInitialization(GameObject gameObject)
        {
            m_AudioSource = gameObject.GetComponent<AudioSource>();
            if (!m_AudioSource)
            {
                m_AudioSource = gameObject.AddComponent<AudioSource>();
                m_AudioSource.playOnAwake = false;
            }
            m_AudioSource.clip = m_AudioClip;
        }

        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_AudioSource = gameObject.GetComponent<AudioSource>();
            m_AudioSource.clip = m_AudioClip;
            m_AudioSource.PlayDelayed(m_Delay);
        }
    }
}