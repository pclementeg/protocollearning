﻿/**
 * The Class BehaviorConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Consequences/BehaviourConsequence")]
    public class BehaviourConsequence : DelayedConsequence
    {
        private LinkedBehavior m_LinkedBehaviour;
        public bool m_EnabledState;

        protected override void SpecificInitialization(GameObject gameObject)
        {
            m_LinkedBehaviour = gameObject.GetComponent<LinkedBehavior>();
            if (!m_LinkedBehaviour)
            {
                m_LinkedBehaviour = gameObject.AddComponent<LinkedBehavior>();
            }
        }

        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_LinkedBehaviour = gameObject.GetComponent<LinkedBehavior>();
            for (int i = 0; i < m_LinkedBehaviour.m_AffectedBehaviors.Length; i++)
            {
                m_LinkedBehaviour.m_AffectedBehaviors[i].enabled = m_EnabledState;
            }
        }
    }
}