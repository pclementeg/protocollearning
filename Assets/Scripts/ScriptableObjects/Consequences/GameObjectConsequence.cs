﻿/**
 * The Class GameObjectConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using System;
using UnityEngine;
using ProtocolLearningData;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Consequences/GameObjectConsequence")]
    public class GameObjectConsequence : DelayedConsequence
    {
        public string m_AffectedGameObjectName;
        public bool m_ActiveState;
        private LinkedGameObject m_LinkedGameObject;

        protected override void SpecificInitialization(GameObject gameObject)
        {
            GameObject[] items = GameObject.FindGameObjectsWithTag(Properties.INTERACTIVE_TAG);
            int objectIndex = Array.FindIndex(items, t => t.name == m_AffectedGameObjectName);
            if (objectIndex >= 0)
            {
                m_LinkedGameObject = gameObject.GetComponent<LinkedGameObject>();
                if (!m_LinkedGameObject)
                {
                    m_LinkedGameObject = gameObject.AddComponent<LinkedGameObject>();
                }
                m_LinkedGameObject.m_AffectedGameObject = items[objectIndex];
            }
            else
            {
                Debug.Log("Object to React not found");
            }
        }

        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_LinkedGameObject = gameObject.GetComponent<LinkedGameObject>();
            m_LinkedGameObject.m_AffectedGameObject.SetActive(m_ActiveState);
        }
    }
}