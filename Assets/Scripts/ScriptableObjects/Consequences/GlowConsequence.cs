﻿/**
 * The Class GlowConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Consequences/GlowConsequence")]
    public class GlowConsequence : Consequence
    {
        public Color m_GlowColor = Color.white;
        public float m_TimeTinted = 1f;
        private Glow m_Glow;

        protected override void SpecificInitialization(GameObject gameObject)
        {
            m_Glow = gameObject.GetComponent<Glow>();
            if (!m_Glow)
            {
                m_Glow = gameObject.AddComponent<Glow>();
            }
            m_Glow.m_WaitTime = m_TimeTinted;
            m_Glow.m_TintedColor = m_GlowColor;
        }

        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_Glow = gameObject.GetComponent<Glow>();
            m_Glow.GlowEffect();
        }
    }
}
