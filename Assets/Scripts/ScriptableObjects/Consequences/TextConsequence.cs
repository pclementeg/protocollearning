﻿/**
 * The Class TextConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Consequences/TextConsequence")]
    public class TextConsequence : Consequence
    {
        public string m_TextTitle;
        public string m_TextBody;
        public Color m_TextTitleColor = Color.white;
        public Color m_TextBodyColor = Color.white;
        public float m_ShowingTime;
        private MenuManager m_MenuManager;


        protected override void SpecificInitialization(GameObject gameObject)
        {
            m_MenuManager = MenuManager.Instance;
        }


        protected override void ImmediateConsequence(GameObject gameObject)
        {
            m_MenuManager.ShowConsequenceText(m_TextTitle, m_TextTitleColor, m_TextBody, m_TextBodyColor, m_ShowingTime);
        }
    }
}
