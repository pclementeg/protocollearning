﻿/**
 * The Class DelayedConsequence
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    public abstract class DelayedConsequence : Consequence
    {
        public float m_DelayTime;
        protected WaitForSeconds m_WaitSeconds;

        public new void Initialization(GameObject gameObject)
        {
            m_WaitSeconds = new WaitForSeconds(m_DelayTime);
            SpecificInitialization(gameObject);
        }

        public new void React(MonoBehaviour monoBehaviour)
        {
            monoBehaviour.StartCoroutine(ReactCoroutine(monoBehaviour.gameObject));
        }


        protected IEnumerator ReactCoroutine(GameObject gameObject)
        {
            yield return m_WaitSeconds;

            ImmediateConsequence(gameObject);
        }
    }
}
