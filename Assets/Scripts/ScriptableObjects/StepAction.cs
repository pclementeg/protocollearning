﻿/**
 * The Class StepAction
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProtocolLearning
{
    //[CreateAssetMenu(menuName = "ProtocolLearning/StepAction")]
    public abstract class StepAction : ScriptableObject
    {
        public string m_TitleDescription;
        public string m_ActionDescription;
        [HideInInspector] public bool m_ActionDone = false;
        [HideInInspector] public bool m_Verified = false;
        public bool m_Repeatable = false;
        public bool m_KeepActive = false;
        public StepAction m_KeepUntilAction;

        public abstract void Initialize(GameObject obj);
        public abstract bool VerifyAction();
        public abstract bool CheckAction(GameObject obj);
    }
}
