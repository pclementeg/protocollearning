﻿/**
 * The Class PunchAction
 * Copyright (C) 2018 Pedro Clemente Garulo - Universidad Internacional de la Rioja
 * 
 * This Program is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://gnu.org/licenses/>.
 * */
using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Actions/PunchAction")]
    public class PunchAction : StepAction
    {
        public string m_ObjetiveName = "Destiny";

        public override void Initialize(GameObject obj)
        {
            if (obj.name != m_ObjetiveName)
            {
                Debug.LogError("Objetive for PunchAction doesn't match with gameObject name");
            }
        }

        public override bool VerifyAction()
        {
            return m_Verified;
        }

        public override bool CheckAction(GameObject obj)
        {
            if (!m_ActionDone || m_Repeatable)
            {
                m_ActionDone = !m_ActionDone;

                return true;
            }
            return false;
        }
    }
}
