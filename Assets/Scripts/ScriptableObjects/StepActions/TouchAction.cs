﻿using UnityEngine;

namespace ProtocolLearning
{
    [CreateAssetMenu(menuName = "ProtocolLearning/Actions/TouchAction")]
    public class TouchAction : StepAction
    {
        public string m_ObjetiveName = "Destiny";

        public override void Initialize(GameObject obj)
        {
            if (obj.name != m_ObjetiveName)
            {
                Debug.LogError("Objetive for TouchAction doesn't match with gameObject name");
            }
        }

        public override bool VerifyAction()
        {
            return m_Verified;
        }

        public override bool CheckAction(GameObject obj)
        {
            if (!m_ActionDone || m_Repeatable)
            {
                m_ActionDone = !m_ActionDone;

                return true;
            }
            return false;
        }
    }
}